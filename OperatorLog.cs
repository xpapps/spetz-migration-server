//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace GpageAPI3
{
    using System;
    using System.Collections.Generic;
    
    public partial class OperatorLog
    {
        public int ID { get; set; }
        public Nullable<int> ServiceCallID { get; set; }
        public int CallTypeID { get; set; }
        public Nullable<int> CallTaskID { get; set; }
        public Nullable<int> ServiceProviderID { get; set; }
        public string SourcePhoneNumber { get; set; }
        public string DestPhoneNumber { get; set; }
        public Nullable<System.DateTime> CallStartDate { get; set; }
        public Nullable<double> CallDuration { get; set; }
        public Nullable<int> CallDisconnectorTypeID { get; set; }
        public Nullable<int> CallStatusID { get; set; }
        public string InputFromDest { get; set; }
        public Nullable<double> ConnectFee { get; set; }
        public System.DateTime CreationDate { get; set; }
        public string Response { get; set; }
        public Nullable<double> RunTimeMilliSeconds { get; set; }
    
        public virtual ServiceProviders ServiceProviders { get; set; }
        public virtual OperatorCallTasks OperatorCallTasks { get; set; }
    }
}
