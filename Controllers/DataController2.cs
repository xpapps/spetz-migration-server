﻿


using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity.Infrastructure;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNet.OData;
using System.Web.Http.Cors;


namespace GpageAPI3.Controllers
{

	public class DataController2
    {

	
    
	     [EnableCors(origins: "*", headers: "*", methods: "*")]
	 public partial class  GetServiceCallSentToController : ODataController { 
	
       

        private bool GetServiceCallSentToExists(int key)
        {
			var db  = new Entities();
            return db.ServiceCallSentTo.Any(e => e.id == key);
        } 

        protected override void Dispose(bool disposing)
        {
			var db  = new Entities();
            db.Dispose();
            base.Dispose(disposing);
        }
   

	   [EnableQuery]
		public IQueryable<ServiceCallSentTo> Get()
		{
			var db  = new Entities();
			return db.ServiceCallSentTo;
		}

		[EnableQuery]
		public SingleResult<ServiceCallSentTo> Get([FromODataUri] int key)
		{
			var db  = new Entities();
			IQueryable<ServiceCallSentTo> result = db.ServiceCallSentTo.Where(e => e.id == key);
			return SingleResult.Create(result);
		}


	/*	[HttpGet]
		public async Task ServiceCallSentTo Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "ServiceCallSentTo");
				await this.OData(spec).Process(this.db);
 
				ServiceCallSentTo entity = db.ServiceCallSentTo.Find(id);
               return entity;
		}*/
	}
	     [EnableCors(origins: "*", headers: "*", methods: "*")]
	 public partial class  GetServiceProvidersSubjecntNAreaController : ODataController { 
	
       

        private bool GetServiceProvidersSubjecntNAreaExists(int key)
        {
			var db  = new Entities();
            return db.ServiceProvidersSubjecntNArea.Any(e => e.id == key);
        } 

        protected override void Dispose(bool disposing)
        {
			var db  = new Entities();
            db.Dispose();
            base.Dispose(disposing);
        }
   

	   [EnableQuery]
		public IQueryable<ServiceProvidersSubjecntNArea> Get()
		{
			var db  = new Entities();
			return db.ServiceProvidersSubjecntNArea;
		}

		[EnableQuery]
		public SingleResult<ServiceProvidersSubjecntNArea> Get([FromODataUri] int key)
		{
			var db  = new Entities();
			IQueryable<ServiceProvidersSubjecntNArea> result = db.ServiceProvidersSubjecntNArea.Where(e => e.id == key);
			return SingleResult.Create(result);
		}


	/*	[HttpGet]
		public async Task ServiceProvidersSubjecntNArea Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "ServiceProvidersSubjecntNArea");
				await this.OData(spec).Process(this.db);
 
				ServiceProvidersSubjecntNArea entity = db.ServiceProvidersSubjecntNArea.Find(id);
               return entity;
		}*/
	}
	     [EnableCors(origins: "*", headers: "*", methods: "*")]
	 public partial class  GetOperatorLogController : ODataController { 
	
       

        private bool GetOperatorLogExists(int key)
        {
			var db  = new Entities();
            return db.OperatorLog.Any(e => e.ID == key);
        } 

        protected override void Dispose(bool disposing)
        {
			var db  = new Entities();
            db.Dispose();
            base.Dispose(disposing);
        }
   

	   [EnableQuery]
		public IQueryable<OperatorLog> Get()
		{
			var db  = new Entities();
			return db.OperatorLog;
		}

		[EnableQuery]
		public SingleResult<OperatorLog> Get([FromODataUri] int key)
		{
			var db  = new Entities();
			IQueryable<OperatorLog> result = db.OperatorLog.Where(e => e.ID == key);
			return SingleResult.Create(result);
		}


	/*	[HttpGet]
		public async Task OperatorLog Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "OperatorLog");
				await this.OData(spec).Process(this.db);
 
				OperatorLog entity = db.OperatorLog.Find(id);
               return entity;
		}*/
	}
	     [EnableCors(origins: "*", headers: "*", methods: "*")]
	 public partial class  GetServiceCallsController : ODataController { 
	
       

        private bool GetServiceCallsExists(int key)
        {
			var db  = new Entities();
            return db.ServiceCalls.Any(e => e.id == key);
        } 

        protected override void Dispose(bool disposing)
        {
			var db  = new Entities();
            db.Dispose();
            base.Dispose(disposing);
        }
   

	   [EnableQuery]
		public IQueryable<ServiceCalls> Get()
		{
			var db  = new Entities();
			return db.ServiceCalls;
		}

		[EnableQuery]
		public SingleResult<ServiceCalls> Get([FromODataUri] int key)
		{
			var db  = new Entities();
			IQueryable<ServiceCalls> result = db.ServiceCalls.Where(e => e.id == key);
			return SingleResult.Create(result);
		}


	/*	[HttpGet]
		public async Task ServiceCalls Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "ServiceCalls");
				await this.OData(spec).Process(this.db);
 
				ServiceCalls entity = db.ServiceCalls.Find(id);
               return entity;
		}*/
	}
	     [EnableCors(origins: "*", headers: "*", methods: "*")]
	 public partial class  GetServiceProvidersController : ODataController { 
	
       

        private bool GetServiceProvidersExists(int key)
        {
			var db  = new Entities();
            return db.ServiceProviders.Any(e => e.id == key);
        } 

        protected override void Dispose(bool disposing)
        {
			var db  = new Entities();
            db.Dispose();
            base.Dispose(disposing);
        }
   

	   [EnableQuery]
		public IQueryable<ServiceProviders> Get()
		{
			var db  = new Entities();
			return db.ServiceProviders;
		}

		[EnableQuery]
		public SingleResult<ServiceProviders> Get([FromODataUri] int key)
		{
			var db  = new Entities();
			IQueryable<ServiceProviders> result = db.ServiceProviders.Where(e => e.id == key);
			return SingleResult.Create(result);
		}


	/*	[HttpGet]
		public async Task ServiceProviders Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "ServiceProviders");
				await this.OData(spec).Process(this.db);
 
				ServiceProviders entity = db.ServiceProviders.Find(id);
               return entity;
		}*/
	}
	     [EnableCors(origins: "*", headers: "*", methods: "*")]
	 public partial class  GetServiceCallStatusController : ODataController { 
	
       

        private bool GetServiceCallStatusExists(int key)
        {
			var db  = new Entities();
            return db.ServiceCallStatus.Any(e => e.ID == key);
        } 

        protected override void Dispose(bool disposing)
        {
			var db  = new Entities();
            db.Dispose();
            base.Dispose(disposing);
        }
   

	   [EnableQuery]
		public IQueryable<ServiceCallStatus> Get()
		{
			var db  = new Entities();
			return db.ServiceCallStatus;
		}

		[EnableQuery]
		public SingleResult<ServiceCallStatus> Get([FromODataUri] int key)
		{
			var db  = new Entities();
			IQueryable<ServiceCallStatus> result = db.ServiceCallStatus.Where(e => e.ID == key);
			return SingleResult.Create(result);
		}


	/*	[HttpGet]
		public async Task ServiceCallStatus Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "ServiceCallStatus");
				await this.OData(spec).Process(this.db);
 
				ServiceCallStatus entity = db.ServiceCallStatus.Find(id);
               return entity;
		}*/
	}
	     [EnableCors(origins: "*", headers: "*", methods: "*")]
	 public partial class  GetCustomerFollowUpActivityController : ODataController { 
	
       

        private bool GetCustomerFollowUpActivityExists(int key)
        {
			var db  = new Entities();
            return db.CustomerFollowUpActivity.Any(e => e.ID == key);
        } 

        protected override void Dispose(bool disposing)
        {
			var db  = new Entities();
            db.Dispose();
            base.Dispose(disposing);
        }
   

	   [EnableQuery]
		public IQueryable<CustomerFollowUpActivity> Get()
		{
			var db  = new Entities();
			return db.CustomerFollowUpActivity;
		}

		[EnableQuery]
		public SingleResult<CustomerFollowUpActivity> Get([FromODataUri] int key)
		{
			var db  = new Entities();
			IQueryable<CustomerFollowUpActivity> result = db.CustomerFollowUpActivity.Where(e => e.ID == key);
			return SingleResult.Create(result);
		}


	/*	[HttpGet]
		public async Task CustomerFollowUpActivity Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "CustomerFollowUpActivity");
				await this.OData(spec).Process(this.db);
 
				CustomerFollowUpActivity entity = db.CustomerFollowUpActivity.Find(id);
               return entity;
		}*/
	}
	     [EnableCors(origins: "*", headers: "*", methods: "*")]
	 public partial class  GetServiceProviderCreditBalanceLOGController : ODataController { 
	
       

        private bool GetServiceProviderCreditBalanceLOGExists(int key)
        {
			var db  = new Entities();
            return db.ServiceProviderCreditBalanceLOG.Any(e => e.ID == key);
        } 

        protected override void Dispose(bool disposing)
        {
			var db  = new Entities();
            db.Dispose();
            base.Dispose(disposing);
        }
   

	   [EnableQuery]
		public IQueryable<ServiceProviderCreditBalanceLOG> Get()
		{
			var db  = new Entities();
			return db.ServiceProviderCreditBalanceLOG;
		}

		[EnableQuery]
		public SingleResult<ServiceProviderCreditBalanceLOG> Get([FromODataUri] int key)
		{
			var db  = new Entities();
			IQueryable<ServiceProviderCreditBalanceLOG> result = db.ServiceProviderCreditBalanceLOG.Where(e => e.ID == key);
			return SingleResult.Create(result);
		}


	/*	[HttpGet]
		public async Task ServiceProviderCreditBalanceLOG Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "ServiceProviderCreditBalanceLOG");
				await this.OData(spec).Process(this.db);
 
				ServiceProviderCreditBalanceLOG entity = db.ServiceProviderCreditBalanceLOG.Find(id);
               return entity;
		}*/
	}


}

}


