﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity.Infrastructure;
using Newtonsoft.Json.Linq;
using Microsoft.AspNetCore.JsonPatch;
using System.Web.Http.Cors;

namespace GpageAPI3.Controllers
{

	public class DataController
    {

	
    
	     



	[EnableCors(origins: "*", headers: "*", methods: "*")]	 
	 public partial class  ServiceCallSentToController : ApiController { 

		[HttpPost]
		
		public ServiceCallSentTo Create(ServiceCallSentTo item)
		{
			   var db = new Entities();
               db.ServiceCallSentTo.Add(item);
			   db.SaveChanges();  
               return item;
		 }


		[HttpPut]
		public ServiceCallSentTo Save(int id, ServiceCallSentTo item)
		{
			var db = new Entities();
			ServiceCallSentTo entity = db.ServiceCallSentTo.Find(id);
			if (entity != null)
			{
				DbEntityEntry<ServiceCallSentTo> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
			    db.SaveChanges();  

			}
			return entity;
		 }


		[HttpPatch]
		public ServiceCallSentTo Update(int id, [FromBody] JsonPatchDocument<ServiceCallSentTo> item)
		{
			//JObject json = JObject.Parse(text);
			var db = new Entities();
			ServiceCallSentTo entity = db.ServiceCallSentTo.Find(id);
			if (entity != null)
			{
				DbEntityEntry<ServiceCallSentTo> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
				db.SaveChanges();
			}
			return entity;
		 }



		[HttpGet]
		public ServiceCallSentTo GetById(int id)
		{
				var db = new Entities();
				ServiceCallSentTo entity = db.ServiceCallSentTo.Find(id);
               return entity;
		}


	/*	[HttpGet]
		public async Task ServiceCallSentTo Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "ServiceCallSentTo");
				await this.OData(spec).Process(this.db);
 
				ServiceCallSentTo entity = db.ServiceCallSentTo.Find(id);
               return entity;
		}*/
	}
	     



	[EnableCors(origins: "*", headers: "*", methods: "*")]	 
	 public partial class  ServiceProvidersSubjecntNAreaController : ApiController { 

		[HttpPost]
		
		public ServiceProvidersSubjecntNArea Create(ServiceProvidersSubjecntNArea item)
		{
			   var db = new Entities();
               db.ServiceProvidersSubjecntNArea.Add(item);
			   db.SaveChanges();  
               return item;
		 }


		[HttpPut]
		public ServiceProvidersSubjecntNArea Save(int id, ServiceProvidersSubjecntNArea item)
		{
			var db = new Entities();
			ServiceProvidersSubjecntNArea entity = db.ServiceProvidersSubjecntNArea.Find(id);
			if (entity != null)
			{
				DbEntityEntry<ServiceProvidersSubjecntNArea> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
			    db.SaveChanges();  

			}
			return entity;
		 }


		[HttpPatch]
		public ServiceProvidersSubjecntNArea Update(int id, [FromBody] JsonPatchDocument<ServiceProvidersSubjecntNArea> item)
		{
			//JObject json = JObject.Parse(text);
			var db = new Entities();
			ServiceProvidersSubjecntNArea entity = db.ServiceProvidersSubjecntNArea.Find(id);
			if (entity != null)
			{
				DbEntityEntry<ServiceProvidersSubjecntNArea> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
				db.SaveChanges();
			}
			return entity;
		 }



		[HttpGet]
		public ServiceProvidersSubjecntNArea GetById(int id)
		{
				var db = new Entities();
				ServiceProvidersSubjecntNArea entity = db.ServiceProvidersSubjecntNArea.Find(id);
               return entity;
		}


	/*	[HttpGet]
		public async Task ServiceProvidersSubjecntNArea Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "ServiceProvidersSubjecntNArea");
				await this.OData(spec).Process(this.db);
 
				ServiceProvidersSubjecntNArea entity = db.ServiceProvidersSubjecntNArea.Find(id);
               return entity;
		}*/
	}
	     



	[EnableCors(origins: "*", headers: "*", methods: "*")]	 
	 public partial class  OperatorLogController : ApiController { 

		[HttpPost]
		
		public OperatorLog Create(OperatorLog item)
		{
			   var db = new Entities();
               db.OperatorLog.Add(item);
			   db.SaveChanges();  
               return item;
		 }


		[HttpPut]
		public OperatorLog Save(int id, OperatorLog item)
		{
			var db = new Entities();
			OperatorLog entity = db.OperatorLog.Find(id);
			if (entity != null)
			{
				DbEntityEntry<OperatorLog> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
			    db.SaveChanges();  

			}
			return entity;
		 }


		[HttpPatch]
		public OperatorLog Update(int id, [FromBody] JsonPatchDocument<OperatorLog> item)
		{
			//JObject json = JObject.Parse(text);
			var db = new Entities();
			OperatorLog entity = db.OperatorLog.Find(id);
			if (entity != null)
			{
				DbEntityEntry<OperatorLog> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
				db.SaveChanges();
			}
			return entity;
		 }



		[HttpGet]
		public OperatorLog GetById(int id)
		{
				var db = new Entities();
				OperatorLog entity = db.OperatorLog.Find(id);
               return entity;
		}


	/*	[HttpGet]
		public async Task OperatorLog Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "OperatorLog");
				await this.OData(spec).Process(this.db);
 
				OperatorLog entity = db.OperatorLog.Find(id);
               return entity;
		}*/
	}
	     



	[EnableCors(origins: "*", headers: "*", methods: "*")]	 
	 public partial class  ServiceCallsController : ApiController { 

		[HttpPost]
		
		public ServiceCalls Create(ServiceCalls item)
		{
			   var db = new Entities();
               db.ServiceCalls.Add(item);
			   db.SaveChanges();  
               return item;
		 }


		[HttpPut]
		public ServiceCalls Save(int id, ServiceCalls item)
		{
			var db = new Entities();
			ServiceCalls entity = db.ServiceCalls.Find(id);
			if (entity != null)
			{
				DbEntityEntry<ServiceCalls> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
			    db.SaveChanges();  

			}
			return entity;
		 }


		[HttpPatch]
		public ServiceCalls Update(int id, [FromBody] JsonPatchDocument<ServiceCalls> item)
		{
			//JObject json = JObject.Parse(text);
			var db = new Entities();
			ServiceCalls entity = db.ServiceCalls.Find(id);
			if (entity != null)
			{
				DbEntityEntry<ServiceCalls> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
				db.SaveChanges();
			}
			return entity;
		 }



		[HttpGet]
		public ServiceCalls GetById(int id)
		{
				var db = new Entities();
				ServiceCalls entity = db.ServiceCalls.Find(id);
               return entity;
		}


	/*	[HttpGet]
		public async Task ServiceCalls Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "ServiceCalls");
				await this.OData(spec).Process(this.db);
 
				ServiceCalls entity = db.ServiceCalls.Find(id);
               return entity;
		}*/
	}
	     



	[EnableCors(origins: "*", headers: "*", methods: "*")]	 
	 public partial class  ServiceProvidersController : ApiController { 

		[HttpPost]
		
		public ServiceProviders Create(ServiceProviders item)
		{
			   var db = new Entities();
               db.ServiceProviders.Add(item);
			   db.SaveChanges();  
               return item;
		 }


		[HttpPut]
		public ServiceProviders Save(int id, ServiceProviders item)
		{
			var db = new Entities();
			ServiceProviders entity = db.ServiceProviders.Find(id);
			if (entity != null)
			{
				DbEntityEntry<ServiceProviders> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
			    db.SaveChanges();  

			}
			return entity;
		 }


		[HttpPatch]
		public ServiceProviders Update(int id, [FromBody] JsonPatchDocument<ServiceProviders> item)
		{
			//JObject json = JObject.Parse(text);
			var db = new Entities();
			ServiceProviders entity = db.ServiceProviders.Find(id);
			if (entity != null)
			{
				DbEntityEntry<ServiceProviders> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
				db.SaveChanges();
			}
			return entity;
		 }



		[HttpGet]
		public ServiceProviders GetById(int id)
		{
				var db = new Entities();
				ServiceProviders entity = db.ServiceProviders.Find(id);
               return entity;
		}


	/*	[HttpGet]
		public async Task ServiceProviders Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "ServiceProviders");
				await this.OData(spec).Process(this.db);
 
				ServiceProviders entity = db.ServiceProviders.Find(id);
               return entity;
		}*/
	}
	     



	[EnableCors(origins: "*", headers: "*", methods: "*")]	 
	 public partial class  ServiceCallStatusController : ApiController { 

		[HttpPost]
		
		public ServiceCallStatus Create(ServiceCallStatus item)
		{
			   var db = new Entities();
               db.ServiceCallStatus.Add(item);
			   db.SaveChanges();  
               return item;
		 }


		[HttpPut]
		public ServiceCallStatus Save(int id, ServiceCallStatus item)
		{
			var db = new Entities();
			ServiceCallStatus entity = db.ServiceCallStatus.Find(id);
			if (entity != null)
			{
				DbEntityEntry<ServiceCallStatus> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
			    db.SaveChanges();  

			}
			return entity;
		 }


		[HttpPatch]
		public ServiceCallStatus Update(int id, [FromBody] JsonPatchDocument<ServiceCallStatus> item)
		{
			//JObject json = JObject.Parse(text);
			var db = new Entities();
			ServiceCallStatus entity = db.ServiceCallStatus.Find(id);
			if (entity != null)
			{
				DbEntityEntry<ServiceCallStatus> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
				db.SaveChanges();
			}
			return entity;
		 }



		[HttpGet]
		public ServiceCallStatus GetById(int id)
		{
				var db = new Entities();
				ServiceCallStatus entity = db.ServiceCallStatus.Find(id);
               return entity;
		}


	/*	[HttpGet]
		public async Task ServiceCallStatus Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "ServiceCallStatus");
				await this.OData(spec).Process(this.db);
 
				ServiceCallStatus entity = db.ServiceCallStatus.Find(id);
               return entity;
		}*/
	}
	     



	[EnableCors(origins: "*", headers: "*", methods: "*")]	 
	 public partial class  CustomerFollowUpActivityController : ApiController { 

		[HttpPost]
		
		public CustomerFollowUpActivity Create(CustomerFollowUpActivity item)
		{
			   var db = new Entities();
               db.CustomerFollowUpActivity.Add(item);
			   db.SaveChanges();  
               return item;
		 }


		[HttpPut]
		public CustomerFollowUpActivity Save(int id, CustomerFollowUpActivity item)
		{
			var db = new Entities();
			CustomerFollowUpActivity entity = db.CustomerFollowUpActivity.Find(id);
			if (entity != null)
			{
				DbEntityEntry<CustomerFollowUpActivity> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
			    db.SaveChanges();  

			}
			return entity;
		 }


		[HttpPatch]
		public CustomerFollowUpActivity Update(int id, [FromBody] JsonPatchDocument<CustomerFollowUpActivity> item)
		{
			//JObject json = JObject.Parse(text);
			var db = new Entities();
			CustomerFollowUpActivity entity = db.CustomerFollowUpActivity.Find(id);
			if (entity != null)
			{
				DbEntityEntry<CustomerFollowUpActivity> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
				db.SaveChanges();
			}
			return entity;
		 }



		[HttpGet]
		public CustomerFollowUpActivity GetById(int id)
		{
				var db = new Entities();
				CustomerFollowUpActivity entity = db.CustomerFollowUpActivity.Find(id);
               return entity;
		}


	/*	[HttpGet]
		public async Task CustomerFollowUpActivity Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "CustomerFollowUpActivity");
				await this.OData(spec).Process(this.db);
 
				CustomerFollowUpActivity entity = db.CustomerFollowUpActivity.Find(id);
               return entity;
		}*/
	}
	     



	[EnableCors(origins: "*", headers: "*", methods: "*")]	 
	 public partial class  ServiceProviderCreditBalanceLOGController : ApiController { 

		[HttpPost]
		
		public ServiceProviderCreditBalanceLOG Create(ServiceProviderCreditBalanceLOG item)
		{
			   var db = new Entities();
               db.ServiceProviderCreditBalanceLOG.Add(item);
			   db.SaveChanges();  
               return item;
		 }


		[HttpPut]
		public ServiceProviderCreditBalanceLOG Save(int id, ServiceProviderCreditBalanceLOG item)
		{
			var db = new Entities();
			ServiceProviderCreditBalanceLOG entity = db.ServiceProviderCreditBalanceLOG.Find(id);
			if (entity != null)
			{
				DbEntityEntry<ServiceProviderCreditBalanceLOG> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
			    db.SaveChanges();  

			}
			return entity;
		 }


		[HttpPatch]
		public ServiceProviderCreditBalanceLOG Update(int id, [FromBody] JsonPatchDocument<ServiceProviderCreditBalanceLOG> item)
		{
			//JObject json = JObject.Parse(text);
			var db = new Entities();
			ServiceProviderCreditBalanceLOG entity = db.ServiceProviderCreditBalanceLOG.Find(id);
			if (entity != null)
			{
				DbEntityEntry<ServiceProviderCreditBalanceLOG> ee = db.Entry(entity);
				ee.CurrentValues.SetValues(item);
				db.SaveChanges();
			}
			return entity;
		 }



		[HttpGet]
		public ServiceProviderCreditBalanceLOG GetById(int id)
		{
				var db = new Entities();
				ServiceProviderCreditBalanceLOG entity = db.ServiceProviderCreditBalanceLOG.Find(id);
               return entity;
		}


	/*	[HttpGet]
		public async Task ServiceProviderCreditBalanceLOG Get()
		{
				var db = new Entities();
				var spec = new TableSpec("table": "ServiceProviderCreditBalanceLOG");
				await this.OData(spec).Process(this.db);
 
				ServiceProviderCreditBalanceLOG entity = db.ServiceProviderCreditBalanceLOG.Find(id);
               return entity;
		}*/
	}


}

}


