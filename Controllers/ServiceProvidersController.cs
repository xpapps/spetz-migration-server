﻿using GpageAPI3.Helpers;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace GpageAPI3.Controllers
{
    public class Provider
    {
        public string ProviderName { get; set; }
        public string BusinessID { get; set; }
        public string BusinessName { get; set; }
        public string ProviderAddress { get; set; }
        public string ProviderEmailAddress { get; set; }
        public string ProviderPassword { get; set; }
        public bool IsActiveOLD { get; set; }
        public Nullable<int> TelephonePrefix { get; set; }
        public string Telephone { get; set; }
        public string[] subjects { get; set; }
        public string[] serviceSubjects { get; set; }
        public int districtId { get; set; }
        public string phone { get; set; }
    }

    public class ServiceProviderController : ApiController
    {

        public string Get(int id)
        {
            return "value";
        }

        public string Post([FromBody]Provider provider)
        {
            var db = new Entities();
            var NewProvider = new ServiceProviders();

            try
            {


                if (isProviderExists(db, provider.ProviderName, provider.BusinessID))
                {
                    return "Provider already exists";
                }

                NewProvider.SmsReceptionVerificationStatus = 3;//sms status VERIFICATION_MESSAGE_SENT
                NewProvider.GetCallMethod = 1;//sms 
                NewProvider.ServiceProviderStatusID = 1; //active
                NewProvider.WorksOnHolidays = true;
                NewProvider.WorksOnFridays = true;
                NewProvider.QualityGroup = 0;
                NewProvider.ShortLeadCount = 0;
                NewProvider.ShortLeadResponseCount = 0;
                NewProvider.DetailedLeadCount = 0;
                NewProvider.isExternalRating = true;
                NewProvider.CustomerRankCount = 0;
                NewProvider.AverageShortLeadResponseTime = 0;
                NewProvider.FalseArrivalReport = 0;
                NewProvider.JOBERTempGrade = 0;
                NewProvider.TermsApprovalDate = DateTime.Now;
                NewProvider.CreationDate = DateTime.Now;
                NewProvider.DiscountID = 0;
                NewProvider.SendInvoiceMethod = 1;
                NewProvider.AgreeToTerms = true;
                NewProvider.Operational = 1;
                NewProvider.SmsReceptionVerificationStatus = 1;
                NewProvider.SMSState = 1;
                NewProvider.AcceptsLeadsOnPhone = true;
                NewProvider.IsSubjectAreaSepareted = false;
                NewProvider.IsSPPay = false;
                NewProvider.EnableAutoCharge = true;
                NewProvider.manualInvoice = false;
                NewProvider.IsActive = 1;
                NewProvider.AutomaticLoginCode = new Guid();
                NewProvider.paysForInvoiceFirst = false;
                NewProvider.LastPremiumGrade = 0;

                ServiceSubjects[] subjectArray = new ServiceSubjects[provider.serviceSubjects.Length];

                PropertyCopier<Provider, ServiceProviders>.Copy(provider, NewProvider);

                int[] subjectIds = GetServiceSubjectIDs(db, provider.serviceSubjects);
                int[] areasIds = GetServiceAreasIDs(db, provider.districtId);


                db.ServiceProviders.Add(NewProvider);

                string[] address = provider.ProviderAddress.Split(',');
                NewProvider.City = address[address.Length - 1];
                Array.Resize(ref address, address.Length - 1);

                NewProvider.ProviderAddress = string.Join(", ", address);

                db.SaveChanges();


                string[] telephoneArray = provider.phone.Split('-');

                string PhonePrefix = telephoneArray[0];

                var PhonePrefixId = db.PhonePrefix.Where(s => s.PhonePrefix1 == PhonePrefix).Select(i => i.ID).FirstOrDefault();

                var ProviderPhone = new ServiceProviderTelephones
                {
                    ServiceProviderID = NewProvider.id,
                    PhonePrefixID = PhonePrefixId,
                    CreationDate = DateTime.Now,
                    PhoneNumber = telephoneArray[1],
                };

                db.ServiceProviderTelephones.Add(ProviderPhone);


                if (NewProvider.id != 0)
                {
                    List<ServiceProvidersSubjecntNArea> providerSubjectAndAreas = new List<ServiceProvidersSubjecntNArea>();

                    SetProviderSubjectAndAreas(providerSubjectAndAreas, db, NewProvider.id, subjectIds, areasIds);

                    if (providerSubjectAndAreas != null)
                    {
                        foreach (ServiceProvidersSubjecntNArea row in providerSubjectAndAreas)
                        {
                            db.ServiceProvidersSubjecntNArea.Add(row);
                        }

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Trace.WriteLine("Cannot add new provider");
            }



            return $"Provider with id {NewProvider.id} was created";
        }

        private bool isProviderExists(Entities context, string pName, string bID)
        {
            var Provider = context.ServiceProviders
                .Where(s => s.ProviderName == pName || s.BusinessID == bID)
                .FirstOrDefault();

            return false;
        }

        private int[] GetServiceSubjectIDs(Entities context, string[] subjectsNameArray)
        {
            int[] serviceSubjectIds = new int[subjectsNameArray.Length];

            for (int i = 0; i < subjectsNameArray.Length; i++)
            {

                var subject = subjectsNameArray[i];

                var populatedSubject = context.ServiceSubjects
                    .Where(s => subject == s.ServiceName)
                    .FirstOrDefault();

                if (populatedSubject != null)
                {
                    serviceSubjectIds[i] = populatedSubject.id;
                }
            }
            return serviceSubjectIds;
        }

        private int[] GetServiceAreasIDs(Entities context, int districtId)
        {
            //int[] serviceAreasIds = new int[subjectsNameArray.Length];

            int[] serviceAreasArray = context.ServiceAreas
                .Where(s => s.ServiceDistrictsID == districtId)
                .Select(s => s.id)
                .ToArray();

            return serviceAreasArray;
        }

        private void SetProviderSubjectAndAreas(List<ServiceProvidersSubjecntNArea> providerSubjectAndAreasRows, Entities context, int providerId, int[] subjectIds, int[] areaIds)
        {
            var subjects = subjectIds.Where(i => i != 0).ToArray();

            var areas = areaIds.Where(i => i != 0).ToArray();

            // var numberOfRows = (subjects.Length * areas.Length);
            string creationDate = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");

            for (int i = 0; i < subjects.Length; i++)
            {
                for (int j = 0; j < areas.Length; j++)
                {
                    providerSubjectAndAreasRows.Add(new ServiceProvidersSubjecntNArea
                    {
                        ServiceProviderID = providerId,
                        ServiceSubjectID = subjects[i],
                        ServiceAreasID = areas[j],
                        CreationDate = Convert.ToDateTime(creationDate)
                    });
                }
            }
        }
    }
}
