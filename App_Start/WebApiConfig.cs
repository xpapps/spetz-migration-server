﻿using Microsoft.AspNet.OData.Builder;
using Microsoft.AspNet.OData.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;

namespace GpageAPI3
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

           
            ODataModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<ServiceCallSentTo>("GetServiceCallSentTo");
            builder.EntitySet<OperatorLog>("GetOperatorLog");

            builder.EntitySet<ServiceCalls>("GetServiceCalls");
            builder.EntitySet<ServiceProviders>("GetServiceProviders");
            builder.EntitySet<CustomerFollowUpActivity>("GetCustomerFollowUpActivity");
            builder.EntitySet<ServiceProviderCreditBalanceLOG>("GetServiceProviderCreditBalanceLOG");
            builder.EntitySet<ServiceProvidersSubjecntNArea>("ServiceProvidersSubjecntNArea");

            builder.EntitySet<ServiceProviderTelephones>("ServiceProviderTelephones");
            builder.EntitySet<PhonePrefix>("PhonePrefix");

            builder.EntitySet<ServiceCallStatus>("GetServiceCallStatus");

            config.Select().Expand().Filter().OrderBy().MaxTop(null).Count();
            config.MapODataServiceRoute(
                routeName: "ODataRoute",
                routePrefix: null,
                model: builder.GetEdmModel());
        }
    }
}
